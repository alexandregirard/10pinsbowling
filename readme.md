#### 10pins

10 pins bowling game - angular module blGame.

####Components

* SASS
* Angular
* Heavy usage of grunt, because I ain't do 1 millions (even 1k actually) the same thing


####Install

We are assuming you have npm, grunt and bower installed.

* Pull repo
* Execute npm install
* Execute bower


###Develop

*Execute "grunt"
*Open browser to http://localhost:8081/


###Insights

* When developing, grunt watch over any changes done on sass files, config/config.tmpl.js, and angular partials
* Grunt serves the app/ folder through http


##Notes

Finish scenario with 3rd throw for the last frame.
Tests service and directives.
