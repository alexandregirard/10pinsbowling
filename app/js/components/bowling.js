(function(){
  'use strict';

  var module = angular.module('bowling', []);

  module.directive('blGame', ['blScoreService',
    function(blScoreService){

      return {
        scope:{
          pinsNb: '@'
        },
        replace: true,
        // transclude: true,
        // template: '<div ng-transclude/>',
        controller: function($scope){
          var self = this;
          var currentFrameIdx; //current played frame;

          $scope.pinsNb = parseInt($scope.pinsNb, 10);
          console.log('$scope.pinsNb', typeof $scope.pinsNb);

          this.addScore = function(score){

            var isLastFrame = false;
            if(currentFrameIdx==$scope.pinsNb){
              isLastFrame = true;
            }
            console.log('isLastFrame idx:'+currentFrameIdx, isLastFrame);

            var val = 0;
            switch(score.label){
              case 'X':
                val = 10;
              break;
              case '/':
                val = $scope.pinsNb - self.scores[currentFrameIdx].throwone; //spare button > compute diff
              break;
              default:
                val = parseInt(score.label, 10);
              break;
            }
            //console.log('score', self.scores);

            var frame = self.scores[currentFrameIdx];

            if(typeof frame==='undefined'){ //first throw of currentFrameIdx

              if(val===10){ //manage strike case
                frame = {
                  throwone: val,
                  throwtwo: val,
                  subtotal: val,
                  desc: 'strike'
                };

                blScoreService.backwardscoring(self.scores, currentFrameIdx, frame);

                self.scores[currentFrameIdx] = frame; //saving
                currentFrameIdx = currentFrameIdx + 1;

              }else{ // open frame
                frame = {
                  throwone: val,
                  throwtwo: undefined,
                  desc: 'open'
                };
                this.frameFirst = val;
                self.scores[currentFrameIdx] = frame; //saving
              }

            }else if(typeof frame.throwtwo==='undefined'){ //second throw of currentFrameIdx
              frame.throwtwo = val;
              frame.subtotal = frame.throwone+frame.throwtwo;
              if(frame.subtotal===10){
                frame.desc = 'spare';
              }

              blScoreService.backwardscoring(self.scores, currentFrameIdx, frame);

              self.scores[currentFrameIdx] = frame; //saving

              //end of frame
              currentFrameIdx = currentFrameIdx + 1;
              this.frameFirst = 0; //init blocking buttons
            }

            //console.log('current frame: '+ currentFrameIdx);
            if(isLastFrame){
              console.log('game over');
            }

            return this; //chaining (by habit)
          };

          this.get = function(key){
            return $scope[key];
          };

          this.reset = function(){
            self.scores     = [];
            self.frameFirst = 0;
            currentFrameIdx = 0;
          };

          //init new game
          this.reset();
        }
      };
    }
  ]);

  module.directive('blScoreBoard', [
    function(){
      return {
        scope: {},
        require: '^blGame',
        template: '<h1>Score</h1><div ng-repeat="i in pinsArray track by $index" class="bowl-frame">'
+'<div ng-if="scores[$index].desc===\'strike\'" class="bowl-frame--throw-one"></div>'
+'<div ng-if="scores[$index].desc!==\'strike\'" class="bowl-frame--throw-one">{{scores[$index].throwone}}</div>'
+'<div class="bowl-frame--throw-two">'
+'<span ng-if="scores[$index].desc===\'strike\'">X</span>'
+'<span ng-if="scores[$index].desc===\'open\'">{{scores[$index].throwtwo}}</span>'
+'<span ng-if="scores[$index].desc===\'spare\'">/</span>'
+'</div>'
+'<div ng-if="scores[$index].throwthree" class="bowl-frame--throw-last">{{scores[$index].throwthree}}</div>'
+'<div class="bowl-frame--total">{{scores[$index].subtotal}}</div>'
+'</div>',
        link: function(scope, el, attrs, ctrl){
          scope.pinsArray = new Array( ctrl.get('pinsNb') || 0 ); //frame number === pin number <= absurd relationship

          scope.$watch(function(){
            return ctrl.scores;
          }, function(val){
            scope.scores = val;
          }, true);
        }
      };
    }
  ]);

  module.directive('blScoreInput', ['$timeout',
    function($timeout){
      return {
        scope: {},
        require: '^blGame',
        link: function(scope, el, attrs, ctrl){

          var pinsNb = parseInt(ctrl.get('pinsNb') || 0, 10 );

          scope.btns = [];

          for(var i=0; i<pinsNb; ++i){
            scope.btns.push({
              label: i,
              disabled: false
            });
          }

          scope.btns.push({
            label: 'X',
            disabled: false
          },{
            label: '/',
            disabled: true
          });

          //disable buttons
          scope.$watch(function(){
            return ctrl.frameFirst;
          }, function(val){
            //console.log('disabled', val);
            if(val===0){
              for(var i=0, l=scope.btns.length;i<l;++i){
                scope.btns[i].disabled = false;
              }
              //disable spare
              scope.btns[pinsNb+1].disabled = true;
              return;
            }
            //second throw
            for(var i=pinsNb-val, l=pinsNb;i<l;++i){
              scope.btns[i].disabled = true;
            }
            //disable strike
            scope.btns[pinsNb].disabled   = true;
            //enable spare
            scope.btns[pinsNb+1].disabled = false;
          });

          //score input
          scope.score = function(btn){
            ctrl.addScore(btn);
          };

          //bind new game
          scope.reset = function(){
            ctrl.reset();
          };

        },
        template: '<button ng-repeat="(key, btn) in btns track by key" ng-disabled="btn.disabled" ng-click="score(btn)">{{btn.label}}</button><button ng-click="reset()" class="primary block">New game</button>'
      };
    }
  ]);

  //scoring service - easier to test
  module.service('blScoreService', [
    function(){
      var self = this;

      //must rewrite the function for unmutable version
      this.backwardscoring = function(scores, currentFrameIdx, frame){
        // compute subtotal getting previous frames
        if(currentFrameIdx>0){

          //manage case previous frame==spare
          if(scores[currentFrameIdx-1].desc==='spare'){ // we add current frame firstthrow
            scores[currentFrameIdx-1].subtotal += frame.throwone;
          }else if(scores[currentFrameIdx-1].desc==='strike'){

            //2 consecutive strikes
            if(currentFrameIdx>1 && scores[currentFrameIdx-2].desc==='strike'){
              scores[currentFrameIdx-2].subtotal += frame.throwone;
              scores[currentFrameIdx-1].subtotal = scores[currentFrameIdx-2].subtotal + scores[currentFrameIdx-1].throwone ; //max 3*pins number (=30 in 10pins game) per frame
            }

            scores[currentFrameIdx-1].subtotal += frame.subtotal;
          }

          frame.subtotal += scores[currentFrameIdx-1].subtotal;
        }
      };
    }
  ]);

})();